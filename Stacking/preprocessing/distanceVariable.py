#-*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import urllib
import urllib2
import json
import BeautifulSoup
import xml.etree.ElementTree as ET
from xml.dom import minidom
#import MySQLdb
import math
import codecs

def getAddress (mapx, mapy) :
    url = 'https://openapi.naver.com/v1/map/reversegeocode.xml?encoding=utf-8&coordType=tm128&query=%s,%s' % (mapx, mapy)

    req = urllib2.Request(url)
    req.add_header('Host', 'openapi.naver.com')
    req.add_header('User-Agent', 'curl/7.49.1')
    req.add_header('Accept', '*/*')
    req.add_header('X-Naver-Client-Id', 'jqA84uQIOs1n9yUR9qDa')
    req.add_header('X-Naver-Client-Secret', 'Rld3zzbmDH')

    response = urllib2.urlopen(req)
    xmldoc = minidom.parse(response)

    address = xmldoc.getElementsByTagName('address')[0].firstChild.nodeValue
    address = urllib.quote(address.encode('utf-8'))

    return address

def getLngLat (address):
    url = 'https://openapi.naver.com/v1/map/geocode.xml?encoding=utf-8&coordType=latlng&query=%s' % (address)
    req = urllib2.Request(url)
    req.add_header('Host', 'openapi.naver.com')
    req.add_header('User-Agent', 'curl/7.49.1')
    req.add_header('Accept', '*/*')
    req.add_header('X-Naver-Client-Id', 'jqA84uQIOs1n9yUR9qDa')
    req.add_header('X-Naver-Client-Secret', 'Rld3zzbmDH')

    response = urllib2.urlopen(req)
    xmldoc = minidom.parse(response)

    x = xmldoc.getElementsByTagName('x')[0].firstChild.nodeValue
    y = xmldoc.getElementsByTagName('y')[0].firstChild.nodeValue

    return x,y

print('Searching..')
#네거리 좌표 가져오기
#url = 'https://openapi.naver.com/v1/search/local.xml?query=%EA%B4%91%EC%A3%BC%20%EC%82%AC%EA%B1%B0%EB%A6%AC&display=100&start=1' #saguri
#url = 'https://openapi.naver.com/v1/search/local.xml?query=%EA%B4%91%EC%A3%BC%20%EC%82%BC%EA%B1%B0%EB%A6%AC&display=100&start=1'#samguri
#url = 'https://openapi.naver.com/v1/search/local.xml?query=%EA%B4%91%EC%A3%BC%20%EC%98%A4%EA%B1%B0%EB%A6%AC&display=100&start=1'#oguri
url = 'https://openapi.naver.com/v1/search/local.xml?query=%EA%B4%91%EC%A3%BC%201%ED%98%B8%EC%84%A0&display=100&start=1' #jihachul

req = urllib2.Request(url)
req.add_header('Host', 'openapi.naver.com')
req.add_header('User-Agent', 'curl/7.49.1')
req.add_header('Accept', '*/*')
req.add_header('X-Naver-Client-Id', 'jqA84uQIOs1n9yUR9qDa')
req.add_header('X-Naver-Client-Secret', 'Rld3zzbmDH')

response = urllib2.urlopen(req)
#print response.readlines()\n",
xmldoc = minidom.parse(response)

print('Search End')

with codecs.open('sub.txt', 'w', encoding='utf-8') as out:
    out.write("title,address,mapx,mapy")
    out.write("\n")

    for i in xmldoc.getElementsByTagName('item'):
        ele1 = i.getElementsByTagName('title')[0].firstChild.nodeValue
        ele1 = ele1.replace('<b>',' ')
        ele1 = ele1.replace('</b>',' ')
        ele2 = i.getElementsByTagName('address')[0].firstChild.nodeValue
        ele3 = i.getElementsByTagName('mapx')[0].firstChild.nodeValue
        ele4 = i.getElementsByTagName('mapy')[0].firstChild.nodeValue
        address = getAddress(ele3, ele4)
        lng,lat = getLngLat(address)
        print ("converting address to lng lat")
        newStr = "%s,%s,%s,%s" % (ele1, ele2, lng, lat)
        print (newStr)
        out.write(newStr)
        out.write("\n")

out.close
