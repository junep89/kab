import numpy
import pandas

from sklearn import tree
from sklearn import linear_model
from sklearn import neighbors
from sklearn import neural_network
from sklearn import ensemble
from sklearn.ensemble import ExtraTreesRegressor

#from Stack_regression import classifyData, Stacking, newDataLoad, loadStackingModel, loadModel, saveModel, trainRegressor
import Stack_regression

#init
Stack_regression.init()
import xgboost

#setting
use_saved_model = False
#variables
num_of_class = 1
num_of_folds = 10
n_trees = 100
seed = 11231
#Class division
#div =  numpy.sqrt(500000)
divs = [500000,2500000]

#Load train data and test data
X_train, Y_train = Stack_regression.newDataLoad(csvPath="data/daejeon/train_trimmed_gakuka.csv", random_state = seed)
X_test, Y_test = Stack_regression.newDataLoad(csvPath="data/daejeon/test_trimmed_gakuka.csv", random_state = seed)


"model setting"
#regressor for classification
classifier = ExtraTreesRegressor(n_estimators = 100, random_state=seed)
# regressors for level 0
clfs = [
        ensemble.ExtraTreesRegressor(n_estimators = n_trees, max_depth=15,  random_state=seed), #
        ensemble.RandomForestRegressor(n_estimators = n_trees, max_depth = 50,
                                        criterion="mse",min_samples_leaf = 2,
                                        min_samples_split = 2,oob_score=False,
                                        n_jobs= -1,
                                        verbose= 0,min_impurity_split = 1.5, random_state=seed),
        ensemble.GradientBoostingRegressor( max_features= 1.0,  learning_rate= 0.1,
                                            max_depth= 6,  min_samples_leaf= 5,
                                            n_estimators=200, random_state=seed),
        xgboost.XGBRegressor( max_depth= 6,n_estimators = 600, learning_rate = 0.05 , seed=seed),
        ensemble.BaggingRegressor(base_estimator = tree.DecisionTreeRegressor() ,
                                  bootstrap_features = True, random_state=seed),
        ensemble.BaggingRegressor(base_estimator = tree.ExtraTreeRegressor() ,n_estimators = n_trees,
                                  bootstrap_features = True, random_state=seed),
        ensemble.AdaBoostRegressor(base_estimator= tree.ExtraTreeRegressor(),random_state=seed)
]
# regressor for level 1
metaRegressor = linear_model.HuberRegressor(epsilon = 1, alpha = 10) #best

print X_train.shape, X_test.shape

"STEP 1 - Classification"
if use_saved_model == True:
    classifier = Stack_regression.loadModel('classifier', './load_this_model')
elif use_saved_model == False:
    classifier = Stack_regression.trainRegressor(classifier, X_train, Y_train)

#separating the training dataset by their class
df_train, df_test = Stack_regression.classifyData(classifier, X_train, Y_train, divs, X_test, Y_test, get_test_set = True)

divClass = len(divs) + 1
X_train_list = []
Y_train_list = []
X_test_list = []
Y_test_list = []

for i in range(0,divClass) :
    trainClass = df_train[df_train['Label'] == str(i) ]
    X_train = trainClass.drop('Label', axis = 1).drop('TargetValue' , axis = 1).drop('TrainResult' , axis = 1)
    Y_train = trainClass['TargetValue']
    X_train_list.append(X_train)
    Y_train_list.append(Y_train)

    testClass = df_test[df_test['Label'] == str(i) ]
    X_test = testClass.drop('Label', axis = 1).drop('TargetValue' , axis = 1).drop('TrainResult' , axis = 1)
    Y_test = testClass['TargetValue']
    X_test_list.append(X_test)
    Y_test_list.append(Y_test)



"STEP 2 - Train stacking model"
# build stacking model for each class
predictVal = []
actualVal = []
for i in range(0, len(X_train_list)):
    print "Stacking - Class", str(i+1)
    className = 'class'+str(i+1)
    if use_saved_model == False:
        clfs, metaregressor = Stack_regression.Stacking(X_train_list[i], Y_train_list[i], X_test_list[i], Y_test_list[i], clfs, metaRegressor, n_folds = num_of_folds)
        Stack_regression.saveModel('./saved_model', className, classifier, clfs, metaregressor)
    else:
        clfs, metaregressor = Stack_regression.loadStackingModel(className, './load_this_model')

    "STEP 3 - Evaluate stacking model"
    #declare empty arrays to fill in
    classified_results = numpy.zeros((len(X_test_list[i]), len(clfs)))
    #Stacking on class
    for c, clf in enumerate(clfs):
        #make prediction
        classified_results[:,c] = clf.predict(X_test_list[i])

    Stack_regression.getFeatureImportance(clfs, className, len(X_train_list[i].columns), printResult = True)
    #combine result
    final_result = metaregressor.predict(classified_results)
    Y_test = Y_test_list[i]
    predictVal = numpy.hstack((predictVal, final_result))
    actualVal = numpy.hstack((actualVal, Y_test))

    stacking_result = numpy.hstack((classified_results, final_result[:, numpy.newaxis], Y_test[:, numpy.newaxis]))
    "test"
    stacking_result = numpy.square(stacking_result)
    numpy.savetxt("./result/stacking_result_"+className+".csv", stacking_result, delimiter=",")

    print 'coef:', metaregressor.coef_

#remove sqrt
predictVal = numpy.square(predictVal)
actualVal = numpy.square(actualVal)

Stack_regression.getResult(predictVal, actualVal)
