package kab.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import kab.dbInterface.MySQLConn;

public class DataUtil {
	//CSV IO

	/**
	 * write list of data to CSV
	 * @param dataList: List<ArrayList<String>> data
	 * @param in: List<String[]> data
	 * @param savePath: 
	 * @return : String to csv format
	 */
	public static void writeCSV(List<ArrayList<String>> dataList, List<String[]> in, String fileName){
		//String savePath = FileUtils.getRootPath()+"/result/"+fileName+/*FileUtils.getCurrentTime()+*/".csv";
		String savePath = fileName + ".csv";
		if(dataList != null && in == null){
			//Received dataList of type List<ArrayList<String>> only.
			
			int num_of_attr = dataList.get(0).size();

			StringBuilder lines = new StringBuilder();
			for(int i = 0; i < dataList.size(); i++){
				ArrayList<String> land = dataList.get(i);
				dataList.set(i, land);

				for ( int j = 0; j < num_of_attr-1 ; j++) { 
					lines.append(land.get(j)).append(",");       
				}

				lines.append(land.get(num_of_attr-1));
				lines.append("\n");
			}
			//TEST
			//System.out.println( lines.toString());
			FileUtils.writeStringToFile(lines.toString(), savePath);
			
		}else if(dataList == null && in != null ){
			//Received dataList of type List<String[]> only.
			
			CSVWriter writer = null;	
			
			try {
				writer = new CSVWriter(new FileWriter(savePath,true));
				writer.writeAll(in);
				writer.flush();
				writer.close();
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else if(dataList == null && in == null){
			//No data received.
			System.out.println("DataUtil.writeCSV: No data received");
		}
		
	}
	/**
	 * Overloading writeCSV method.
	 * @param filename: filename
	 * @param array: list to be saved
	 */
	public static void writeCSV(ArrayList<String> dataList, String filename){
		
		try {
			BufferedWriter out;

			out = new BufferedWriter(new FileWriter(new File(filename)));
			for(String s : dataList){			
						out.write(s);
						out.newLine();
			}
			out.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Reads csv file from the given path
	 * @param path : path of input csv file.
	 * @param dataList : data list.
	 * @param inHeader: header of the data list by its name.
	 * @param inHeaderByVal: header of the data list by its position.
	 */
	public static void readCSV(String path, List<String[]> dataList, Hashtable<String, Integer> inHeader, Hashtable<Integer, String> inHeaderByVal){
		//Read file here
		boolean readHeader = true;
		CSVReader reader = null;
		System.out.println("Read "+path+" file");
		try{
			reader = new CSVReader(new FileReader(path));

			String[] data;
			//Get current line
			while((data=reader.readNext())!=null){
				//Read csv file and store them in an array list.
				//Store the headers in a hash table.
				
				if(readHeader){
					for(int i=0; i< data.length; i++){
						inHeader.put(data[i], i);
						inHeaderByVal.put(i, data[i]);
					}
					readHeader = false;
				}else{
					if(inHeader.contains("SBUN1")){
						//add leading zeros to make 4 digits
						data[inHeader.get("SBUN1")] = StringUtils.leftPad(data[inHeader.get("SBUN1")], 4, "0");
						data[inHeader.get("SBUN2")] = StringUtils.leftPad(data[inHeader.get("SBUN2")], 4, "0");
					}
					
				}
				
				dataList.add(data);

			}
			readHeader= true;
			
			
		}catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Finished reading csv file");
	}
	public static void readKSJGfile(String path, List<String[]> dataList, Hashtable<String, Integer> headers, Hashtable<Integer, String> headersByIndex) throws IOException{
		BufferedReader br = new BufferedReader(new FileReader(path));
		boolean readHeader = true;
		System.out.println("Read "+path+" file");
		
		for(String line; (line=br.readLine()) !=null;){
			String[] lineSplit = line.split(",");
			if(readHeader){
				for(int i=0; i<lineSplit.length; i++){
					
					headers.put(lineSplit[i], i);
					headersByIndex.put(i, lineSplit[i]);
				}
				
			}else{
			
				lineSplit[3] = StringUtils.leftPad(lineSplit[3], 4,"0");
				lineSplit[4] = StringUtils.leftPad(lineSplit[4], 4,"0");
			}
			dataList.add(lineSplit);
			readHeader = false;
		}
		br.close();
		System.out.println("Finished reading csv file");
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//DB IO
	
	/**
	 * TODO: Set headers
	 * Join tables by sending queries to DB
	 * @param conn
	 * @return ArrayList<String[]> joinedData
	 */
	public static ArrayList<String[]> getJoinedTable(Connection conn){
		boolean readHeader = true;
		ResultSet rs = null;
		java.sql.Statement sqlSt = null;
		ArrayList<String[]> inputData = new ArrayList<String[]>();

		String joinQueryPath = FileUtils.getRootPath()+"/query/join_query.txt";
		String joinQuery = FileUtils.readStringFromFile(joinQueryPath);
		
		try { 
			sqlSt = conn.createStatement();
			rs = MySQLConn.getResultsFromDB(joinQuery, conn, sqlSt);
			
			ResultSetMetaData resultSetMetaData = rs.getMetaData();
			int columnCount = resultSetMetaData.getColumnCount();
			
			while(rs.next()){
				String[] values = new String[columnCount];

				for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {
					if(readHeader){
						values[i - 1] = resultSetMetaData.getColumnName(i);
					}
					else 
						values[i - 1] = rs.getString(i);
				}
				readHeader = false;
				inputData.add(values);
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally{
			try {sqlSt.close();} catch (SQLException e) {e.printStackTrace();}
			try {rs.close();} catch (SQLException e) {e.printStackTrace();}
		}
		return inputData;
	}
	
	/**
	 * TODO: make method for headers
	 * Get table from DB
	 * @param conn : connection object
	 * @param tableName : table name
	 * @return ArrayList<String[]> data
	 */
	public static ArrayList<String[]> getInputDataFromDB(Connection conn, String tableName){
		System.out.println("Start querying from DB");
		boolean readHeader = true;
		ResultSet rs = null;
		java.sql.Statement sqlSt = null;
		ArrayList<String[]> inputData = new ArrayList<String[]>();

		String query = "SELECT * FROM "+tableName;

		try { 
			sqlSt = conn.createStatement();
			rs = MySQLConn.getResultsFromDB(query, conn, sqlSt);

			ResultSetMetaData resultSetMetaData = rs.getMetaData();
			int columnCount = resultSetMetaData.getColumnCount();

			while(rs.next()){
				String[] values = new String[columnCount];

				for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {
					if(readHeader){
						values[i - 1] = resultSetMetaData.getColumnName(i);
					}
					else 
						values[i - 1] = rs.getString(i);
				}
				readHeader = false;
				inputData.add(values);
			}

		}catch (SQLException e) {
			System.out.println("[WARNING] there is no '"+tableName+"' table in database!");
			e.printStackTrace();
			
		}finally{
			System.out.println("Finished getting data from DB");
			try {sqlSt.close();} catch (SQLException e) {e.printStackTrace();}
			try {rs.close();} catch (SQLException e) {e.printStackTrace();}
		}
		return inputData;
	}
	
	/**
	 * Overloading method.
	 * Get table from DB
	 * @param conn : connection object
	 * @param tableName : table name
	 * @return ArrayList<String[]> data
	 * @throws SQLException 
	 */
	public static ArrayList<String[]> getInputDataFromDB(Connection conn, String tableName, Hashtable<String, Integer> headers, Hashtable<Integer,String> headersByIndex) throws SQLException{
		System.out.println("Start querying from DB");
		boolean readHeader = true;
		

		ArrayList<String[]> inputData = new ArrayList<String[]>();
		String query = "SELECT * FROM "+tableName;

		
		Statement sqlSt = conn.createStatement();
		ResultSet rs = sqlSt.executeQuery(query);
		System.out.println("Retrieved Data from DB");
		
		

		while(rs.next()){
			
			ResultSetMetaData resultSetMetaData = rs.getMetaData();
			int columnCount = resultSetMetaData.getColumnCount();
			String[] values = new String[columnCount];
			for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {
				if(readHeader){
					
					values[i - 1] = resultSetMetaData.getColumnName(i);
					headers.put(resultSetMetaData.getColumnName(i), i - 1);
					headersByIndex.put(i - 1, resultSetMetaData.getColumnName(i));
					
				}
				else {
					values[i - 1] = rs.getString(i);
					
				}
			}
			readHeader = false;
			inputData.add(values);
		}
		
	
		return inputData;
	}
	
	/**
	 * Overloading method.
	 * Get table from DB
	 * @param conn : connection object
	 * @param tableName : table name
	 * @return 
	 * @return ArrayList<String[]> data
	 * @throws SQLException 
	 */
	public static ArrayList<String> getKSJGFromDB(Connection conn, String tableName, Hashtable<String, Integer> headers, Hashtable<Integer, String> headersByIndex) throws SQLException{
		System.out.println("Start querying from DB");
		ArrayList<String> al = new ArrayList<String>();
		boolean readHeader = true;
		
 	    String query = "SELECT SREG,SEUB,SSAN,SBUN1,SBUN2,year,PYOJUNJI,AREA,GIMOK,GIMOK2,YOUNGDO,GIYUK,GIYUK2,Y1AREA,Y2AREA,DIST,FASC,FASCY,GITA,GITA2USE,GITA3,GITA4,GUBUN,OKDO,SURI,LIMYA,GOJEU,HUNG,BAN,JUB,LENGTH,TRA,HASU,HANDWORK,BIKYO,PYOJUN2,GAKUKA,GAKUKJ,GAKUKD,GAKUKS,GAKUKY,BIG_PROJ,BIG_STEP,LAND_GRP" 
 	    		 + " FROM kirc_kab.ksjg_processed";
 	    		 
 	      Statement st = conn.createStatement();
 	      ResultSet rs = st.executeQuery(query);
 	     
 	      System.out.println("Retrieved data from sql");
 	      //Set headers
 	      
 	      
 	      while (rs.next())
 	      {
 	    	  String line = "";
 	    	  ResultSetMetaData meta = rs.getMetaData();
 	          int numberOfColumns = meta.getColumnCount() ; 
 	          for (int i = 1 ; i < numberOfColumns + 1 ; i ++ ) {
 	        	 if(readHeader){
 	        		String s = meta.getColumnName(i);
					headers.put(meta.getColumnName(i), i - 1);
					headersByIndex.put(i - 1, meta.getColumnName(i));
					line += s + ",";
 	        	 }else{
 	        		 String s = rs.getString(i).replace("\r", "");
 	 	        	 line += s + ",";
 	        	 }
 	          }
 	         readHeader = false;
 	         line = line.substring(0,line.length()-1);
 	         // System.out.println(line);
 
 	         al.add(line);
 	      }
 	      System.out.println("END");

 	      return al;
	}
	
	/**
 	 * Auxiliary method for matchKSJG filter.
 	 * @param filename
 	 * @param array
 	 */
 	public static void writeArray(String filename, ArrayList<String> array){
 		
 		try {
 			BufferedWriter out;
 
 			out = new BufferedWriter(new FileWriter(new File(filename)));
 			for(String s : array){			
 						out.write(s);
 						out.newLine();
 			}
 			out.close();
 
 		} catch (IOException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
 	}
	
	
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//List manipulation
	
	/**
	 * Convert List<String[]> type to  List<List<String>> type
	 * @param dataList: list to be converted
	 * @return
	 */
	public static ArrayList<ArrayList<String>> convertInnerArray2List(List<String[]> dataList){
		ArrayList<ArrayList<String>> convertedList = new ArrayList<ArrayList<String>>();

		for(int i = 0; i < dataList.size(); i++){
			ArrayList<String> land = new ArrayList<String>(Arrays.asList(dataList.get(i)));
			convertedList.add(land);
		}
		return convertedList;
	}
	
	/**
	 * Deep copy from srcList to destList.
	 * @param srcList
	 * @param destList
	 * @param inHeader
	 */
	public static void copyLists(List<String[]> srcList, List<String[]> destList){
		destList.clear();
		
		for(int i=0; i<srcList.size(); i++){
			destList.add(srcList.get(i));
		}
	}
	
	/**
	 * Print Hash Map
	 * @param headerTable
	 */
	public static void printHashMap(HashMap<String, Integer> headerTable){
		Iterator<String> iterator = headerTable.keySet().iterator();
		while (iterator.hasNext()) {
			String key = (String) iterator.next();
			System.out.println(key+" "+headerTable.get(key));
		}
	}

	/**
	 * Print list of type List<ArrayList<String>>
	 * @param dataList
	 */
	public static void printDataListList(List<ArrayList<String>> dataList){

		for(int i = 0; i < dataList.size(); i++){
			ArrayList<String> data = dataList.get(i);

			for ( int j = 0; j <  data.size() ; j++) { 
				System.out.print(data.get(j)+" ");    
			}
			System.out.println();
		}
	}

	/**print list of type List<String>
	 * @param dataList
	 */
	public static void printList(List<String> dataList){

		for(String data : dataList){
			System.out.println(data);
		}
	}
	/**
	 * Print string[] records
	 * @param array
	 * @return
	 */
	public static String printArray(String[] array){
		String st = "";
		for(String s: array){
			st += s+",";
			System.out.print(s + " " );
		}
		st= st.substring(0, st.length()-1);
		System.out.println();
		return st;
	}
	
	
}
