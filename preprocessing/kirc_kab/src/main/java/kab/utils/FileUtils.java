package kab.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class FileUtils {
	/**get root path*/
	public static String getRootPath() {
	
			File file = new File(".");
			String result = "";
	
			try{
				result = file.getCanonicalPath().toString();
			}catch(Exception e){
				System.out.println("exception getRootPath = " + e.toString());
			}
			return result;
		}
	
	/**write input string to file*/
	public static void writeFile(String path, String text){
		 try
	        {
	            FileWriter fw = new FileWriter(path); // 절대주소 경로 가능
	            BufferedWriter bw = new BufferedWriter(fw);
	            
	            bw.write(text);
	            bw.newLine();
	             
	            bw.close();
	        }
	        catch (IOException e)
	        {
	            e.printStackTrace();
	        }
	}
	/**read string form file*/
	public static String readStringFromFile(String path) {
		
		String text = "";
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String line;
			while ((line = br.readLine()) != null) {
				text += (line+"\n");
			}
			br.close();
			br = null;

		} catch (IOException e) {
			e.printStackTrace();
		}
		return text;
	}

	/**get current time in string. append it to filename when save it.*/
	public static String getCurrentTime(){
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMdd_HHMMssSSS");
		return dateFormat.format(calendar.getTime());
	}
	
	/**write input string to file*/
	public static void writeStringToFile(String text, String path) {

		try {
			deleteFile(path);
			BufferedWriter bw = new BufferedWriter(new FileWriter(path));
			bw.write(text);
			bw.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	/**delete file on the path*/
	public static boolean deleteFile(String filePath) {

		boolean result = false;

		try {
			File target = new File(filePath);

			if (target.exists()) {
				target.delete();
				result = true;
			}
		} catch (Exception e) {
			System.out.println("ex delete file =" + e.toString());
			result = false;
		}
		return result;
	}
}

