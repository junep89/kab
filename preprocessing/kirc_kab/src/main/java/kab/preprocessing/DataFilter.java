package kab.preprocessing;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

import java.sql.*;
import kab.utils.DataUtil;
import kab.utils.TimeUtil;
import kab.constants.DBConstants;
import kab.constants.PropertyPriceRateConstants;

/**
 * This class contains methods for filtering data.
 * @author kirc_kab
 *
 */

public class DataFilter extends kab.dbInterface.MySQLConn{
	
	
	//List to be used for filtering
	private static List<String[]> editedMasterDataList = new ArrayList<String[]>();

	public static void main(String args[]) {
	

	}
	
	/**
	 * Invoke this method to run filters on data.
	 * @param conn : DB connection
	 * @param masterDataInput : List<String[]> data list
	 * @param headers: Hashtable<Column labels , record positions> headers
	 * @return
	 * @throws IOException 
	 * @throws SQLException 
	 */
	public static List<String[]> getFilteredData (Connection conn, List<String[]> masterDataInput, Hashtable<String, Integer> headers, Hashtable<Integer, String> headersByIndex, String ksjgData, Hashtable<String, Integer> ksjgHeaders, Hashtable<Integer, String> ksjgHeadersByIndex) throws SQLException, IOException {
	
		//Deep copy from masterDataList to editedMasterDataList for list manipulation
		DataUtil.copyLists(masterDataInput, editedMasterDataList);

		
		//DataUtil.printHashMap(headers);
		
		//Apply filters from here
		System.out.println("실거래가 존재 필지 필터 실행: " + realTransactionAmtFilter(masterDataInput, headers));
		System.out.println("각 필지 동일년도 1회거래 필터 실행: " + oneTransactionFilter(masterDataInput, headers));
		System.out.println("공시지가 존재여부 필터: " + matchKSJG(conn,masterDataInput, headers, headersByIndex, ksjgData, ksjgHeaders, ksjgHeadersByIndex));
	
		System.out.println("Total Area>= 1 필터 실행: " + totalAreaFilter(masterDataInput, headers));
		System.out.println("이용상황  필터 실행: " + youngdoFilter(masterDataInput, headers));
		System.out.println("지역2  필터 실행: " + giyuk2Filter(masterDataInput, headers));
		System.out.println("Y2area  필터 실행: " + Y2AreaFilter(masterDataInput, headers));
		System.out.println("표준지2  필터 실행: " + pyojun2Filter(masterDataInput, headers));
		//System.out.println("Land_GRP  필터 실행: " + landGRPFilter(masterDataInput, headers));
		System.out.println("jibunGBN  필터 실행: " + jibunGBNFilter(masterDataInput, headers));
		System.out.println("실거래면적/공시지가면적 비율  필터 실행: " + areaRatioFilter(masterDataInput, headers));
		//System.out.println("좌표  필터 실행: " + coordsFilter(masterDataInput, headers));
		System.out.println("지형지세  필터 실행: " + topographyFilter(masterDataInput, headers));
		System.out.println("2014년까지 지분거래 필터 실행: " + dealymdANDJibunFilter(masterDataInput, headers));
		System.out.println("2015년 이후 지분거래 필터 실행: " + dealymdANDJibunRatioFilter(masterDataInput, headers));
		System.out.println("공부상 면적 필터 실행: " + areaFilter(masterDataInput, headers));
		System.out.println("용도지역 2개이상 필터 실행: " + giyukAndAreaFilter(masterDataInput, headers));
		System.out.println("수작업된 특수토지 필터 실행: " + handworkANDYoungdoFilter(masterDataInput, headers));
		System.out.println("시점수정 실행: " + timeShiftFilter(masterDataInput, headers));
		DataUtil.writeCSV(null, masterDataInput, "filtered");
		
		return masterDataInput;
	}
	
	/**
	 * Filter condition 1
	 * Filter for OBJ_AMT.
	 */
	public static boolean realTransactionAmtFilter(List<String[]> dataList, Hashtable<String, Integer> headers){
		System.out.println("실거래가 존재 필지 filter started");
		editedMasterDataList.clear();
		editedMasterDataList.add(dataList.get(0));// add header
		
		//실거래가 == OBJ_AMT
		try{
			
			for(int i=1; i<dataList.size(); i++){
				
				if(dataList.get(i)[headers.get("OBJ_AMT")].equals("") ||dataList.get(i)[headers.get("OBJ_AMT")]==null){
					//editedMasterDataList.remove(i);
				}else{
					editedMasterDataList.add(dataList.get(i));
				}
			}
			
			//Deep copy from editedMasterDataList to masterDataList
			DataUtil.copyLists(editedMasterDataList,dataList);
			
			System.out.println("실거래가 존재 필지 filter ended");
			System.out.println("Edited Num Records: " + editedMasterDataList.size());
			System.out.println("Num Records: " + dataList.size());
			
			return true;
			
		}catch(java.lang.NullPointerException e){
			e.printStackTrace();
			return false;
		}
		
		
	}
	
	/**
	 * Filter condition 2
	 * Remove property with more than 1 transaction.
	 */
	public static boolean oneTransactionFilter(List<String[]> modifiedMasterData, Hashtable<String, Integer> headers){
		
		System.out.println("각 필지에1회 거래된 데이터 추출 filter started");
		
		//DEALYMD + SREG + SEUB + SSAN+ SBUN1 + SBUN2 = key
		
		try{
			//HashMap to store records temporarily
			HashMap<String, String[]> records = new HashMap<String, String[]>();
			
			//Add to HashMap
			//Skip headers
			for(int i=1; i<editedMasterDataList.size();i++){
				
				//Curr record
				String[] record = editedMasterDataList.get(i);
				
				String year = record[headers.get("deal_ymd2")];
				String sreg = record[headers.get("SREG")];
				String seub = record[headers.get("SEUB")];
				String ssan = record[headers.get("SSAN")];
				String sbun1 = StringUtils.leftPad(record[headers.get("SBUN1")],4,"0");
				String sbun2 = StringUtils.leftPad(record[headers.get("SBUN2")],4,"0");
		
				String key = year+sreg+seub+ssan+sbun1+sbun2;
				
				if(records.containsKey(key)){
					//remove transactions occurred more than once in the same year on the same property.
					records.remove(key);
				}else{
					records.put(key, record);
				}
				
			}
			//Shallow copy from hashmap to list
			editedMasterDataList = editedMasterDataList.subList(0, 1); // Leave headers and clear the rest
			List<String[]> recordList = new ArrayList<String[]>(records.values());// Create list from map
			editedMasterDataList.addAll(recordList);//Copy to list
			
			//Deep copy from editedMasterDataList to masterDataList
			DataUtil.copyLists(editedMasterDataList,modifiedMasterData);
			
			
			System.out.println("각 필지에1회 거래된 데이터 추출 filter ended");
			System.out.println("Num Records: " + modifiedMasterData.size());
			
			return true;
			
		}catch(java.lang.NullPointerException e){
			e.printStackTrace();
			return false;
		}
		
		
		
	}
	
	
	/**
	 * Filter condition 3
	 * Match with KSJG data.
	 * @throws IOException 
	 * @throws SQLException 
	 */
	public static boolean matchKSJG(Connection conn, List<String[]> modifiedMasterData, Hashtable<String, Integer> headers, Hashtable<Integer, String> headersByIndex, String ksjgData, Hashtable<String, Integer> ksjgHeaders, Hashtable<Integer, String> ksjgHeadersByIndex) throws SQLException, IOException{
		HashMap<String, String[]> reviewList = new HashMap<String, String[]>();
		HashMap<String, HashMap<Integer,String>> ksjgList = getHM(ksjgData, ksjgHeaders, ksjgHeadersByIndex);
		ArrayList<Integer> recordToDelete = new ArrayList<Integer>();
		
		//Loop through master List to find null values
		System.out.println("MatchKSJG filter started");
		
		
		for(int i=1; i<modifiedMasterData.size(); i++){
			for(int j=0; j<headers.size(); j++){
				
				if(modifiedMasterData.get(i)[j] == null){
					//System.out.println("Found null record");
					String sreg = modifiedMasterData.get(i)[headers.get("SREG")];
					String seub = modifiedMasterData.get(i)[headers.get("SEUB")];
					String ssan = modifiedMasterData.get(i)[headers.get("SSAN")];
					String sbun1 = StringUtils.leftPad(modifiedMasterData.get(i)[headers.get("SBUN1")],4,"0");
					String sbun2 = StringUtils.leftPad(modifiedMasterData.get(i)[headers.get("SBUN2")],4,"0");
					String pnuCode = sreg+seub+ssan+sbun1+sbun2;
					//System.out.println("Null pnu: " + pnuCode);
					
					if(!reviewList.containsKey(pnuCode)){
						//This one has null values
						
						//Integer = year , String = record line
						HashMap<Integer, String> currKsjgRecord = ksjgList.get(pnuCode);
						if(currKsjgRecord!=null){
							for(int key: currKsjgRecord.keySet()){
								Integer year = Integer.valueOf(modifiedMasterData.get(i)[headers.get("deal_ymd2")]);
								int diff = year - key;
								//System.out.println("Year is : "+year);
								if(Math.abs(diff) == 1){
									String[] recordVal = currKsjgRecord.get(key).split(",");
									
									for(int k=j; k<headers.size(); k++){
										
									
										String label = headersByIndex.get(k);
										//System.out.println("Null Value: "+ label);
										if(label.equals("PNU")){
											
											editedMasterDataList.get(i)[k] = pnuCode;
										}else if(ksjgHeaders.get(label) == null){
											//System.out.println("Header is not in KSJG : " + label);
										}else{
											int ksjgLabelLocation = ksjgHeaders.get(label);
											//System.out.println("ksjgHeader location: " + ksjgLabelLocation + ", HeaderName: " + ksjgHeadersByIndex.get(ksjgLabelLocation));
											
											if(recordVal.length >= ksjgLabelLocation ){
												editedMasterDataList.get(i)[k] = recordVal[ksjgLabelLocation];
											}else{
												recordToDelete.add(i);
												//System.out.println(label + "is null in KSJG");
											}
										}
										
									
									}
								}
							}
							
						}else{
							recordToDelete.add(i);
							//editedMasterDataList.remove(i);
							//System.out.println("No record found in KSJG");
						}
						
						reviewList.put(pnuCode, modifiedMasterData.get(i));
					}
					
				}
				
			}
			
		}
		deleteRecord(modifiedMasterData, recordToDelete);
		
		DataUtil.copyLists(editedMasterDataList, modifiedMasterData);
		System.out.println("MatchKSJG filter ended");
		System.out.println("Num Records: " + modifiedMasterData.size());
		
		//Print for test purpose
		//writeCSVfile("nonNulljoined.csv", masterDataList);
		
		return true;
	}
	
	/**
	 * Auxiliary method for matchKSJG.
	 * This method deletes specified records in a list.
	 * @param dataList: original data list.
	 * @param delColNumList: List of indices to delete
	 */
	private static void deleteRecord(List<String[]> dataList, ArrayList<Integer> delColNumList){
		List<String[]> newDataList = new ArrayList<String[]>();
		
		for(int i=0; i<dataList.size(); i++){
			if(delColNumList.contains(i)){
				continue;
			}else{
				newDataList.add(editedMasterDataList.get(i));
			}
		}
		
		DataUtil.copyLists(newDataList, editedMasterDataList);
		
		
	}
	
	/**
	 * TODO:
	 * Filter condition 4
	 * if Total Area >=1 else remove
	 */
	public static boolean totalAreaFilter(List<String[]> modifiedMasterData, Hashtable<String, Integer> headers){
		System.out.println("Total Area >=1 filter started");
		editedMasterDataList.clear();
		editedMasterDataList.add(modifiedMasterData.get(0));// add header
		
		try{
			//Loop through masterDataList
			for(int i=1; i < modifiedMasterData.size(); i++){
				String[] currRecord = modifiedMasterData.get(i);
				
				if(currRecord[headers.get("TOT_AREA")].equals("")
						|| currRecord[headers.get("TOT_AREA")]==null
						||Float.valueOf(currRecord[headers.get("TOT_AREA")]) < 1){
						
				}else{
					editedMasterDataList.add(currRecord);
				}
			}
			
			
			DataUtil.copyLists(editedMasterDataList,modifiedMasterData);
			System.out.println("Total Area >=1 filter ended");
			System.out.println("Num Records: " + modifiedMasterData.size());
			return true;
		}catch(java.lang.NullPointerException e){
			e.printStackTrace();
			return false;
		}
	}
	
	
	/**
	 * TODO:
	 * Filter condition 5
	 * if Youngdo >= 800 remove
	 */
	public static boolean youngdoFilter(List<String[]> modifiedMasterData, Hashtable<String, Integer> headers){
		System.out.println("Youngdo filter started");
		editedMasterDataList.clear();
		editedMasterDataList.add(modifiedMasterData.get(0));// add header
		
		try{
			//Loop through masterDataList
			//System.out.println("masterSize: " + modifiedMasterData.size() );
			for(int i=1; i < modifiedMasterData.size(); i++){
				String[] currRecord = modifiedMasterData.get(i);
				//System.out.println("editedSize: " + editedMasterDataList.size() );
				if(currRecord[headers.get("YOUNGDO")]==null||currRecord[headers.get("YOUNGDO")].equals("")
						||Double.valueOf(currRecord[headers.get("YOUNGDO")]) >= 800 ){
					
				}else{
					editedMasterDataList.add(currRecord);
				}
			}
			
			
			DataUtil.copyLists(editedMasterDataList,modifiedMasterData);
			System.out.println("Youngdo filter ended");
			System.out.println("Num Records: " + modifiedMasterData.size());
			return true;
		}catch(java.lang.NullPointerException e){
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * TODO:
	 * Filter condition 6
	 * if not GIYUK2 ==0 remove
	 */
	public static boolean giyuk2Filter(List<String[]> modifiedMasterData, Hashtable<String, Integer> headers){
		System.out.println("GIKYUK2 filter started");
		editedMasterDataList.clear();
		editedMasterDataList.add(modifiedMasterData.get(0));// add header
		
		
		try{
			//Loop through masterDataList
			for(int i=1; i < modifiedMasterData.size(); i++){
				String[] currRecord = modifiedMasterData.get(i);
				if(currRecord[headers.get("GIYUK2")]==null
						||currRecord[headers.get("GIYUK2")].equals("")
						||Double.valueOf(currRecord[headers.get("GIYUK2")]) != 0){
					
				}else{
					editedMasterDataList.add(currRecord);
				}
			}
			
			
			DataUtil.copyLists(editedMasterDataList,modifiedMasterData);
			System.out.println("GIYUK2 filter ended");
			System.out.println("Num Records: " + modifiedMasterData.size());
			return true;
		}catch(java.lang.NullPointerException e){
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * TODO:
	 * Filter condition 7
	 * if not Y2Area ==0 remove
	 */
	public static boolean Y2AreaFilter(List<String[]> modifiedMasterData, Hashtable<String, Integer> headers){
		System.out.println("Y2Area filter started");
		editedMasterDataList.clear();
		editedMasterDataList.add(modifiedMasterData.get(0));// add header
		
		
		try{
			//Loop through masterDataList
			for(int i=1; i < modifiedMasterData.size(); i++){
				String[] currRecord = modifiedMasterData.get(i);
				if(currRecord[headers.get("Y2AREA")]== null
						||currRecord[headers.get("Y2AREA")].equals("")
						||Double.valueOf(currRecord[headers.get("Y2AREA")]) != 0){
					
				}else{
					editedMasterDataList.add(currRecord);
				}
			}
			
			
			DataUtil.copyLists(editedMasterDataList,modifiedMasterData);
			System.out.println("Y2Area filter ended");
			System.out.println("Num Records: " + modifiedMasterData.size());
			return true;
		}catch(java.lang.NullPointerException e){
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * TODO:
	 * Filter condition 8
	 * if not Pyojun2 ==0 remove
	 */
	public static boolean pyojun2Filter(List<String[]> modifiedMasterData, Hashtable<String, Integer> headers){
		System.out.println("Pyojun2 filter started");
		editedMasterDataList.clear();
		editedMasterDataList.add(modifiedMasterData.get(0));// add header
		
		
		try{
			//Loop through masterDataList
			for(int i=1; i < modifiedMasterData.size(); i++){
				String[] currRecord = modifiedMasterData.get(i);
				if(currRecord[headers.get("PYOJUN2")]== null
						||currRecord[headers.get("PYOJUN2")].equals("")
						||Double.valueOf(currRecord[headers.get("PYOJUN2")]) != 0){
					
				}else{
					editedMasterDataList.add(currRecord);
				}
			}
			
			
			DataUtil.copyLists(editedMasterDataList,modifiedMasterData);
			System.out.println("Pyojun2 filter ended");
			System.out.println("Num Records: " + modifiedMasterData.size());
			return true;
		}catch(java.lang.NullPointerException e){
			e.printStackTrace();
			return false;
		}
	}
	
	
	/**
	 * TODO:
	 * Filter condition 9
	 * if not (Land GRP ==0 or empty) remove
	 */
	public static boolean landGRPFilter(List<String[]> modifiedMasterData, Hashtable<String, Integer> headers){
		System.out.println("landGRP filter started");
		editedMasterDataList.clear();
		editedMasterDataList.add(modifiedMasterData.get(0));// add header
		
		
		try{
			//Loop through masterDataList
			for(int i=1; i < modifiedMasterData.size(); i++){
				String[] currRecord = modifiedMasterData.get(i);
				
				if(currRecord[headers.get("LAND_GRP")]==null
						||currRecord[headers.get("LAND_GRP")].equals("") 
						||currRecord[headers.get("LAND_GRP")].equals("0")){
					editedMasterDataList.add(currRecord);
				}
			}
			
			
			DataUtil.copyLists(editedMasterDataList,modifiedMasterData);
			System.out.println("landGRP filter ended");
			System.out.println("Num Records: " + modifiedMasterData.size());
			return true;
		}catch(java.lang.NullPointerException e){
			e.printStackTrace();
			return false;
		}
	}
	
	
	/**
	 * TODO:
	 * Filter condition 10
	 * if not JIBUN GBN ==0 remove
	 */
	public static boolean jibunGBNFilter(List<String[]> modifiedMasterData, Hashtable<String, Integer> headers){
		System.out.println("jibunGBN filter started");
		editedMasterDataList.clear();
		editedMasterDataList.add(modifiedMasterData.get(0));// add header
		
		try{
			
			//Loop through masterDataList
			for(int i=1; i < modifiedMasterData.size(); i++){
				String[] currRecord = modifiedMasterData.get(i);
				if(currRecord[headers.get("JIBUN_GBN")].equals("0")){
					editedMasterDataList.add(currRecord);
				}
			}
			
			DataUtil.copyLists(editedMasterDataList,modifiedMasterData);
			System.out.println("jibunGBN filter ended");
			System.out.println("Num Records: " + modifiedMasterData.size());
			return true;
		}catch(java.lang.NullPointerException e){
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * TODO:
	 * Filter condition 11
	 * if TOT_AREA /AREA <0.9 remove
	 */
	public static boolean areaRatioFilter(List<String[]> modifiedMasterData, Hashtable<String, Integer> headers){
		System.out.println("areaRatio filter started");
		editedMasterDataList.clear();
		editedMasterDataList.add(modifiedMasterData.get(0));// add header
		
		
		try{
			//Loop through masterDataList
			for(int i=1; i < modifiedMasterData.size(); i++){
				String[] currRecord = modifiedMasterData.get(i);
				//System.out.println("Tot area: " + currRecord[headers.get("TOT_AREA")] + ", Area: " + currRecord[headers.get("AREA")]);
				if(currRecord[headers.get("TOT_AREA")] == null ||currRecord[headers.get("AREA")] == null){
					//editedMasterDataList.add(currRecord);
				}
				else if((Double.valueOf(currRecord[headers.get("TOT_AREA")]) / Double.valueOf(currRecord[headers.get("AREA")])) >= 0.9){
					editedMasterDataList.add(currRecord);
				}
			}
			
			
			DataUtil.copyLists(editedMasterDataList,modifiedMasterData);
			System.out.println("areaRatio filter ended");
			System.out.println("Num Records: " + modifiedMasterData.size());
			return true;
		}catch(java.lang.NullPointerException e){
			e.printStackTrace();
			return false;
		}
	}
	
	
	/**
	 * TODO:
	 * Filter condition 13
	 * if no Coords remove
	 */
	public static boolean coordsFilter(List<String[]> modifiedMasterData, Hashtable<String, Integer> headers){
		System.out.println("coords filter started");
		editedMasterDataList.clear();
		editedMasterDataList.add(modifiedMasterData.get(0));// add header
		
		try{
			
			//Loop through masterDataList
			for(int i=1; i < modifiedMasterData.size(); i++){
				String[] currRecord = modifiedMasterData.get(i);
				System.out.println("lng: " + currRecord[headers.get("lng")] + ", lat: " + currRecord[headers.get("lat")]);
				
				if(currRecord[headers.get("lng")]==null||currRecord[headers.get("lat")]==null||currRecord[headers.get("lng")].equals("") ||currRecord[headers.get("lat")].equals("")){
					//editedMasterDataList.remove(i);
				}else{
					editedMasterDataList.add(currRecord);
				}
			}
			
			DataUtil.copyLists(editedMasterDataList,modifiedMasterData);
			System.out.println("coords filter ended");
			System.out.println("Num Records: " + modifiedMasterData.size());
			return true;
		}catch(java.lang.NullPointerException e){
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * TODO:
	 * Filter condition 14
	 * if no topographic data remove
	 */
	public static boolean topographyFilter(List<String[]> modifiedMasterData, Hashtable<String, Integer> headers){
		System.out.println("topography filter started");
		editedMasterDataList.clear();
		editedMasterDataList.add(modifiedMasterData.get(0));// add header
		
		try{
			
			//Loop through masterDataList
			for(int i=1; i < modifiedMasterData.size(); i++){
				String[] currRecord = modifiedMasterData.get(i);
				if(currRecord[headers.get("SLOPE")]==null ||currRecord[headers.get("SLOPE")].equals("")
						||currRecord[headers.get("ELEVATION")]==null||currRecord[headers.get("ELEVATION")].equals("")
						||currRecord[headers.get("RECT_IDX")]==null||currRecord[headers.get("RECT_IDX")].equals("")
						||currRecord[headers.get("CONV_IDX")]==null||currRecord[headers.get("CONV_IDX")].equals("")
						||currRecord[headers.get("WL_IDX")]==null||currRecord[headers.get("WL_IDX")].equals("")
						||currRecord[headers.get("DIST_RAIL")]==null||currRecord[headers.get("DIST_RAIL")].equals("")
						||currRecord[headers.get("DIST_ROAD")]==null||currRecord[headers.get("DIST_ROAD")].equals("")
						||currRecord[headers.get("DIST_SCRAP")]==null||currRecord[headers.get("DIST_SCRAP")].equals("")
						||currRecord[headers.get("Dist_SUB")]==null||currRecord[headers.get("Dist_SUB")].equals("")
						){
					//editedMasterDataList.remove(i);
				}else{
					editedMasterDataList.add(currRecord);
				}
			}
			
			DataUtil.copyLists(editedMasterDataList,modifiedMasterData);
			System.out.println("topography filter ended");
			System.out.println("Num Records: " + modifiedMasterData.size());
			return true;
		}catch(java.lang.NullPointerException e){
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * TODO:
	 * Filter condition 15
	 * DEAL_YMD2 <2015 AND JIBUN_GBN == 1
	 */
	public static boolean dealymdANDJibunFilter(List<String[]> modifiedMasterData, Hashtable<String, Integer> headers){
		System.out.println("dealymdANDJibun filter started");
		editedMasterDataList.clear();
		editedMasterDataList.add(modifiedMasterData.get(0));// add header
		
		try{
			//Loop through masterDataList
			for(int i=1; i < modifiedMasterData.size(); i++){
				String[] currRecord = modifiedMasterData.get(i);
				if(Integer.valueOf(currRecord[headers.get("DEAL_YMD")].substring(0, 4)) < 2015 && currRecord[headers.get("JIBUN_GBN")].equals("1")){
					//editedMasterDataList.remove(i);
				}else{
					editedMasterDataList.add(currRecord);
				}
			}
			
			
			DataUtil.copyLists(editedMasterDataList,modifiedMasterData);
			System.out.println("dealymdANDJibun filter ended");
			System.out.println("Num Records: " + modifiedMasterData.size());
			return true;
		}catch(java.lang.NullPointerException e){
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * TODO:
	 * Filter condition 16
	 * DEAL_YMD2 >=2015 AND jibun_num / jibun deno <0.9
	 */
	public static boolean dealymdANDJibunRatioFilter(List<String[]> modifiedMasterData, Hashtable<String, Integer> headers){
		System.out.println("dealymdANDJibunRatio filter started");
		editedMasterDataList.clear();
		editedMasterDataList.add(modifiedMasterData.get(0));// add header
		
		try{
			//Loop through masterDataList
			for(int i=1; i < modifiedMasterData.size(); i++){
				String[] currRecord = modifiedMasterData.get(i);
				if(Integer.valueOf(currRecord[headers.get("DEAL_YMD")].substring(0, 4)) >= 2015 
						&& (Double.valueOf(currRecord[headers.get("JIBUN_NUME")]) / Double.valueOf(currRecord[headers.get("JIBUN_DENO")])) <0.9 ){
					//editedMasterDataList.remove(i);
				}else{
					editedMasterDataList.add(currRecord);
				}
			}
			
			
			DataUtil.copyLists(editedMasterDataList,modifiedMasterData);
			System.out.println("dealymdANDJibunRatio filter ended");
			System.out.println("Num Records: " + modifiedMasterData.size());
			return true;
		}catch(java.lang.NullPointerException e){
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * TODO:
	 * Filter condition 17
	 * Area <0
	 */
	public static boolean areaFilter(List<String[]> modifiedMasterData, Hashtable<String, Integer> headers){
		System.out.println("area filter started");
		editedMasterDataList.clear();
		editedMasterDataList.add(modifiedMasterData.get(0));// add header
		
		try{
			//Loop through masterDataList
			for(int i=1; i < modifiedMasterData.size(); i++){
				String[] currRecord = modifiedMasterData.get(i);
				if(currRecord[headers.get("AREA")]==null || Double.valueOf(currRecord[headers.get("AREA")])< 0){
					//editedMasterDataList.remove(i);
				}else{
					editedMasterDataList.add(currRecord);
				}
			}
			
			
			DataUtil.copyLists(editedMasterDataList,modifiedMasterData);
			System.out.println("area filter ended");
			System.out.println("Num Records: " + modifiedMasterData.size());
			return true;
		}catch(java.lang.NullPointerException e){
			e.printStackTrace();
			return false;
		}
	}
	
	
	
	/**
	 * TODO:
	 * Filter condition 18
	 * if GIYUK2 == 0 AND y2AREA!=0 Remove
	 */
	public static boolean giyukAndAreaFilter(List<String[]> modifiedMasterData, Hashtable<String, Integer> headers){
		System.out.println("giyukAndArea filter started");
		editedMasterDataList.clear();
		editedMasterDataList.add(modifiedMasterData.get(0));// add header
		
		try{
			//Loop through masterDataList
			for(int i=1; i < modifiedMasterData.size(); i++){
				String[] currRecord = modifiedMasterData.get(i);
				//System.out.println("GIYUK2: " + currRecord[headers.get("GIYUK2")] + ", y2area: " + currRecord[headers.get("Y2AREA")]);
				
				if((currRecord[headers.get("GIYUK2")].equals("0") && 
						Double.valueOf(currRecord[headers.get("Y2AREA")]) != 0)
						||currRecord[headers.get("GIYUK2")]==null 
						||currRecord[headers.get("Y2AREA")]==null ){
					//editedMasterDataList.remove(i);
				}else{
					editedMasterDataList.add(currRecord);
				}
			}
			
			
			DataUtil.copyLists(editedMasterDataList,modifiedMasterData);
			System.out.println("giyukAndArea filter ended");
			System.out.println("Num Records: " + modifiedMasterData.size());
			return true;
		}catch(java.lang.NullPointerException e){
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * TODO:
	 * Filter condition 19
	 * HANDWORK == 1 AND YOUNGDO >=800
	 */
	public static boolean handworkANDYoungdoFilter(List<String[]> modifiedMasterData, Hashtable<String, Integer> headers){
		System.out.println("handworkANDYoungdo filter started");
		editedMasterDataList.clear();
		editedMasterDataList.add(modifiedMasterData.get(0));// add header
		
		try{
			
			//Loop through masterDataList
			for(int i=1; i < modifiedMasterData.size(); i++){
				String[] currRecord = modifiedMasterData.get(i);
				if((currRecord[headers.get("HANDWORK")].equals("1") && 
						(currRecord[headers.get("YOUNGDO")].substring(0, 1).equals("8") ||
								currRecord[headers.get("YOUNGDO")].substring(0, 1).equals("9"))) || currRecord[headers.get("HANDWORK")]==null ||currRecord[headers.get("YOUNGDO")]==null  ){
					//editedMasterDataList.remove(i);
				}else{
					editedMasterDataList.add(currRecord);
				}
			}
			
			
			DataUtil.copyLists(editedMasterDataList,modifiedMasterData);
			System.out.println("handworkANDYoungdo filter ended");
			System.out.println("Num Records: " + modifiedMasterData.size());
			return true;
		}catch(java.lang.NullPointerException e){
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * TODO:
	 * Filter condition 20
	 * Time shift.
	 */
	public static boolean timeShiftFilter(List<String[]> modifiedMasterData, Hashtable<String, Integer> headers){
		System.out.println("timeShift filter started");
		//Array List for property rates
		List<String[]> propertyDataList = new ArrayList<String[]>();
		
		//Header tables
		Hashtable<String, Integer> propertyRateHeaders = new Hashtable<String, Integer>();
		Hashtable<Integer, String> propertyRateHeadersByVal = new Hashtable<Integer, String>();
		
		//Flags
		boolean outputHeader = true;
		
		
		//Read property price rate file
		DataUtil.readCSV("PropertyPriceRate.csv", propertyDataList,propertyRateHeaders,propertyRateHeadersByVal);
		editedMasterDataList.clear();
		
		try{
			
			//Loop through masterDataList
			//Get important variables
			for(int i=0; i<modifiedMasterData.size(); i++){
				if(i==0){
					//Set new headers
					if(outputHeader){
						String[] headerOut = new String[headers.size()+1];
						for(int j =0; j< headers.size(); j++){
							headerOut[j] = modifiedMasterData.get(i)[j];
						}

						headerOut[headers.size()] = DBConstants.COLUMN_NAME_ADJ_OBJ_AMT;
						editedMasterDataList.add(headerOut);
						outputHeader = false;
					}
				}else{
					
				
					String currSREG = modifiedMasterData.get(i)[headers.get("SREG")];
					String currDEALYMD = modifiedMasterData.get(i)[headers.get("DEAL_YMD")];
					String objAmt = modifiedMasterData.get(i)[headers.get("OBJ_AMT")];
					String youngdo = modifiedMasterData.get(i)[headers.get("YOUNGDO")];
					String giyuk = modifiedMasterData.get(i)[headers.get("GIYUK")];
					String giyuk2 = modifiedMasterData.get(i)[headers.get("GIYUK2")];
		
					//Adjusted Price
					double tempPrice = Double.valueOf(objAmt) * (1.0 + getFinalRate(propertyDataList, propertyRateHeaders, currSREG, currDEALYMD, youngdo, giyuk, giyuk2)/100);
					
					String[] output = new String[headers.size()+1];
					//Output file data
					for(int j =0; j< headers.size(); j++){
						output[j] = modifiedMasterData.get(i)[j];
					}
					
					
					output[headers.size()] = String.valueOf(Math.round(tempPrice));
					editedMasterDataList.add(output);

				}
			}
			
			
			DataUtil.copyLists(editedMasterDataList,modifiedMasterData);
			System.out.println("timeShift filter ended");
			System.out.println("Num Records: " + modifiedMasterData.size());
			return true;
		}catch(java.lang.NullPointerException e){
			e.printStackTrace();
			return false;
		}
	}

	
	
		
	
	
	/**
	 * Method for the TimeShiftFilter.
	 * Calculates final rate for land price.
	 * @param sreg
	 * @param dealymd
	 * @param youngdo
	 * @param giyuk
	 * @param giyuk2
	 * @return
	 */
	private static double getFinalRate(List<String[]> propertyDataList, Hashtable<String, Integer> propertyRateHeaders ,String sreg, String dealymd, String youngdo, String giyuk, String giyuk2){
		/*
		 * TODO: calculate the final property rate
		 */
		
		double rate = 0; //Final rate
	
		//Break down year, month, day for simplicity
		String dealYear = dealymd.substring(0,4);
		String dealMonth = dealymd.substring(4,6);
		String dealDay = dealymd.substring(6,8);
		//System.out.println("Deal Date: "+ dealYear +dealMonth+dealDay);
		
		String adjustYear = PropertyPriceRateConstants.getAdjustDate().substring(0, 4);
		String adjustMonth = PropertyPriceRateConstants.getAdjustDate().substring(4,6);
		String adjustDay = PropertyPriceRateConstants.getAdjustDate().substring(6,8);
		System.out.println("Adjust Date: " + adjustYear+adjustMonth+adjustDay);
		
		String region = PropertyPriceRateConstants.DAEJEONREGIONS.get(sreg);
		String propClass;

		if(youngdo.equals("0")){
			propClass = PropertyPriceRateConstants.PROPERTYCLASS.get(giyuk2);
		}else{
			propClass = PropertyPriceRateConstants.PROPERTYCLASS.get(giyuk);
		}
		//System.out.println("지역: "+ region + " 용도지역: " + propClass);
		
		
		//기준시점 month 변동률 계산
		if(dealYear.equals(adjustYear)){
			//Deal year == adjust year
			int dealM = Integer.valueOf(dealMonth);
			double rate_dealM = getPropertyRate(propertyDataList, propertyRateHeaders, region, propClass,dealYear,dealMonth);
			double rate_dealMtemp=0; 
			
			if(dealDay.equals("01")){
				rate_dealMtemp = rate_dealM;
			}else{
				rate_dealMtemp= rate_dealM * Integer.valueOf(dealDay)/TimeUtil.getMaxDays(dealymd);
			}
			
			//get month difference
			int diffMonths = TimeUtil.getMonthDiff(dealymd, PropertyPriceRateConstants.getAdjustDate());
			double rate_adjustM = getPropertyRate(propertyDataList, propertyRateHeaders,region, propClass,adjustYear, adjustMonth);
			double rate_adjustTemp = 0;
			double rate_final=0;
			
			dealM++;
			for(int i=1; i<diffMonths; i++,dealM++){
				rate_adjustTemp += getPropertyRate(propertyDataList, propertyRateHeaders,region,propClass,dealYear,String.valueOf(dealM));
			}
			rate_final = rate_dealMtemp + rate_adjustTemp + rate_adjustM * Integer.valueOf(adjustDay)/TimeUtil.getMaxDays(PropertyPriceRateConstants.getAdjustDate());
			rate = rate_final;
			//System.out.println("Testing more than one month: 0.3007  ---> " + rate_final);
			
		}else{

			//deal year != adjust year
			int dealM = Integer.valueOf(dealMonth);
			double rate_dealM = getPropertyRate(propertyDataList, propertyRateHeaders,region, propClass,dealYear,dealMonth);
			double rate_dealMtemp=0; 
			
			if(dealDay.equals("01")){
				rate_dealMtemp = rate_dealM;
			}else{
				rate_dealMtemp= rate_dealM * Integer.valueOf(dealDay)/TimeUtil.getMaxDays(dealymd);
			}
			
			//Get month difference
			int diffMonths = TimeUtil.getMonthDiff(dealymd, PropertyPriceRateConstants.getAdjustDate());
			double rate_adjustM = getPropertyRate(propertyDataList, propertyRateHeaders,region, propClass,adjustYear, adjustMonth);
			double rate_adjustTemp = 0;
			double rate_final=0;
			
			dealM++;
			for(int i=1; i<diffMonths; i++,dealM++){
				if(dealM == 13){
					dealM = 1;
					int tempYr = Integer.valueOf(dealYear);
					tempYr++;
					dealYear = String.valueOf(tempYr);
				}
				rate_adjustTemp += getPropertyRate(propertyDataList, propertyRateHeaders,region,propClass,dealYear,String.valueOf(dealM));
			}
			rate_final = rate_dealMtemp + rate_adjustTemp + rate_adjustM * Integer.valueOf(adjustDay)/TimeUtil.getMaxDays(PropertyPriceRateConstants.getAdjustDate());
			rate = rate_final;
			System.out.println("Testing more than one month:  " );
		}

		return rate;
	}
	
	/**
	 * Auxiliary method for TimeShiftFilter.
	 * Returns property rate for a specific date.
	 * @param region
	 * @param propClass
	 * @param dealYear
	 * @param dealMonth
	 * @return
	 */
	private static double getPropertyRate(List<String[]> propertyDataList, Hashtable<String, Integer> propertyRateHeaders, String region, String propClass, String dealYear, String dealMonth){
		double rate = 0;
		dealMonth = StringUtils.leftPad(dealMonth,2,"0");
		//System.out.println("region: " + region+" propClass: " + propClass + " dealyear: " + dealYear + " deal Month: " + dealMonth);

		for(int i=1; i<propertyDataList.size();i++){
			//Compare 지역
			if(propertyDataList.get(i)[propertyRateHeaders.get("지 역")].equals(region)){
				//Compare 용도지역
				if(propertyDataList.get(i)[propertyRateHeaders.get("용도지역")].equals(propClass)){
//					System.out.println("-------------" + propClass);
//					System.out.println(dealYear+dealMonth);
//					System.out.println(propertyRateHeaders.get(dealYear+dealMonth));
//					System.out.println();
					String s = propertyDataList.get(i)[propertyRateHeaders.get(dealYear+dealMonth)];
					if(s.equals(null) || s.equals("")){
						rate = 0;
					} else {
						rate = Double.valueOf(propertyDataList.get(i)[propertyRateHeaders.get(dealYear+dealMonth)]);
					}
				}
			}
		}
		
		return rate;
	}
	
	
	/**
	 * Get hash map<year, record>
	 * @param filename
	 * @return
	 * @throws IOException
	 */
	private static HashMap<String, HashMap<Integer,String>> getHM(String filename, Hashtable<String, Integer> ksjgHeaders, Hashtable<Integer, String> ksjgHeadersByIndex) throws IOException{
		 HashMap<String, HashMap<Integer,String>> hm = new HashMap<String, HashMap<Integer,String>>();
		 boolean readHeader = true;
		 
		 BufferedReader br = new BufferedReader(new FileReader(filename));
			for(String line;(line = br.readLine()) != null;){
				if(readHeader){
					readHeader = false;
					String[] lineSplit = line.split(",");
					for(int i=0; i<lineSplit.length; i++){
						ksjgHeaders.put(lineSplit[i], i);
						ksjgHeadersByIndex.put(i, lineSplit[i]);
					}
				}else{
					String[] lineSplit = line.split(",");
					lineSplit[3] = StringUtils.leftPad(lineSplit[3], 4, "0");
					lineSplit[4] = StringUtils.leftPad(lineSplit[4], 4, "0");
					
					String pnuCode = lineSplit[0] +lineSplit[1]+lineSplit[2]+lineSplit[3]+lineSplit[4];
					//System.out.println(pnuCode);
					int year = Integer.parseInt(lineSplit[5]);
					if(hm.get(pnuCode) == null){
						HashMap<Integer, String> inHM = new HashMap<Integer, String>();
						inHM.put(year, line);
						hm.put(pnuCode, inHM);
					} else {
						HashMap<Integer, String> inHM = hm.get(pnuCode);
						inHM.put(year, line);
						hm.put(pnuCode, inHM);	
					}
				}
				
			}
		br.close();	
		
		return hm;
	}
	
}
