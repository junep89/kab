package kab.samples;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import kab.constants.DBConstants;
import kab.dbInterface.MySQLConn;
import kab.preprocessing.DataFilter;
import kab.updateData.AddDistAttr;
import kab.utils.DataUtil;

public class PreprocMain extends MySQLConn{

	/*test data*/
	//private static ArrayList<String[]> testLandList = new ArrayList<String[]>();

	public static void main(String[] args) {
		/**Data on memory **/
		//joined table
		List<String[]> joinedData = new ArrayList<String[]>();
		Hashtable<String, Integer> joinedDataHeaders = new Hashtable<String,Integer>();
		Hashtable<Integer, String> joinedDataHeadersByIndex = new Hashtable<Integer,String>();
		
		//KSJG table
		Hashtable<String, Integer> ksjgDataHeaders = new Hashtable<String, Integer>();
		Hashtable<Integer, String> ksjgDataHeadersByIndex = new Hashtable<Integer, String>();
		
		try {
			//Get connection from MySQLConn
			System.out.println("DB connection made");
			Connection conn = MySQLConn.connect("143.248.91.197:3306", "kirc_kab_debug");//For debugging purpose
			//Connection conn = MySQLConn.connect("143.248.91.197:3306", "kirc_kab");
			
			//1. Join tables by sending queries to DB
			//ArrayList<String[]> dataInput = DataUtil.getJoinedTable(conn);
			
			//2. Get joined table from DB
			joinedData = DataUtil.getInputDataFromDB(conn, DBConstants.TABLE_NAME_JOINED, joinedDataHeaders, joinedDataHeadersByIndex);
			//Store ksjg data as a csv file.
			File checkExistence = new File("KSJGprocessed.csv");
			if(checkExistence.exists() == false){
				DataUtil.writeArray("KSJGprocessed.csv",DataUtil.getKSJGFromDB(conn, DBConstants.TABLE_NAME_KSJG, ksjgDataHeaders, ksjgDataHeadersByIndex));
			}
			

			//3. Get filtered data
			List<String[]> filteredData = DataFilter.getFilteredData(conn, 
					joinedData, joinedDataHeaders, joinedDataHeadersByIndex, 
					"KSJGprocessed.csv", ksjgDataHeaders, ksjgDataHeadersByIndex);
			
			System.out.println("Finished Filtering data. list size: " + filteredData.size());
			
			//4. AddDistAttr
			//Convert list structure
			List<ArrayList<String>> landList = DataUtil.convertInnerArray2List(filteredData);
			
			AddDistAttr ad = new AddDistAttr(conn, landList);
			landList = ad.modifiedLandList;
			//landList = AddDistAttr.addDistAttr(conn, landList, joinedDataHeaders, joinedDataHeadersByIndex);
			
			//5. Add target value
			landList = addTargetValue(landList, joinedDataHeaders, joinedDataHeadersByIndex);
			
			//6. Save the results in CSV or DB
			String fileName = "masterdata";
			DataUtil.writeCSV(landList, null, fileName);
		
			MySQLConn.closeConnection();
			System.out.println("DB connection closed");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
		
	
	/**
	 * append target value to the data
	 * @return modified data list
	 */
	private static List<ArrayList<String>> addTargetValue(List<ArrayList<String>> dataList, Hashtable<String, Integer> headers, Hashtable<Integer, String> headersByIndex){
		
		int index_OBJ_AMT = -1;
		try{
			index_OBJ_AMT = headers.get(DBConstants.COLUMN_NAME_ADJ_OBJ_AMT);
		}catch (Exception e) {
			//use obj_amt if there is no adjusted abj_amt
			index_OBJ_AMT = headers.get(DBConstants.COLUMN_NAME_OBJ_AMT);
		}
		int index_TOT_AREA = headers.get("TOT_AREA");
		
		/*add header*/
		ArrayList<String> header = dataList.get(0);
		header.add(DBConstants.COLUMN_NAME_TARGET_VALUE);
		dataList.set(0, header);
		
		
		for(int i = 1; i < dataList.size(); i++){
			ArrayList<String> data = dataList.get(i);
			
			double obj_amt = Double.parseDouble(data.get(index_OBJ_AMT));
			double tot_area = Double.parseDouble(data.get(index_TOT_AREA));
			
			double target_val = obj_amt/tot_area;
			
			data.add(Double.toString(target_val));
			dataList.set(i, data);
		}
		
		//TODO: update header
		headers.put(DBConstants.COLUMN_NAME_TARGET_VALUE, headers.size());
		headersByIndex.put(headersByIndex.size(), DBConstants.COLUMN_NAME_TARGET_VALUE);
		
		return dataList;
	}
	

	

	

}