package kab.updateData;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import kab.constants.DBConstants;
import kab.dbInterface.MySQLConn;

public class AddressToPosition {
	
	
	public static int keyNum = 3;
	public static String[] clientID= {
			"jqA84uQIOs1n9yUR9qDa", //yusik 애플리케이션 클라이언트 아이디값";
			"azYTfvCtcIA_jykEDb0U",//jayong 애플리케이션 클라이언트 아이디값";
			"GzBVdybgnNeq5aDrfuTW",//umi 애플리케이션 클라이언트 아이디값";
			"g5hnQ54hI69PVwn4l6h7"};//jason 애플리케이션 클라이언트 아이디값";
	public static String[] clientSecret = {
			"Rld3zzbmDH",
			"T7wHxNARBO",
			"DK9NP_wg7h",
			"2hhD_8tYlq"
	};
	
	
	/**
	 * Converts address in korean to latitude and longitude
	 * 
	 * @param korAddress: address in korean
	 * @return : double [latitude, longitude]
	 */
	public static double [] getAdressToPosition(String korAddress) {
        
		//StringBuilder stringBuilder = null;
		JSONObject position = null;
		double[] retVal = null;
		
		try{
				String addr = URLEncoder.encode(korAddress, "UTF-8");
	            String apiURL = "https://openapi.naver.com/v1/map/geocode?encoding=utf-8&coordType=latlng&query="+addr; //json
	          	//String apiURL = "http://openapi.map.naver.com/api/geocode?key=93729c17a174f9acea28ba33a17984ef&encoding=utf-8&coord=latlng&output=json&query=" + korAddress; 
	            URL url = new URL(apiURL);
	            HttpURLConnection con = (HttpURLConnection)url.openConnection();
	            con.setRequestMethod("GET");
	            con.setRequestProperty("X-Naver-Client-Id", clientID[keyNum]);
	            con.setRequestProperty("X-Naver-Client-Secret", clientSecret[keyNum]);
	            int responseCode = con.getResponseCode();
	            BufferedReader br;
	           // System.out.println("ResponseCode: " + responseCode);
	            
	            
	            if(responseCode==200) { // 정상 호출
	                br = new BufferedReader(new InputStreamReader(con.getInputStream()));
	            } else if(responseCode==404){
	            	retVal = new double[2];
	            	retVal[0] = Double.MAX_VALUE;
	        		retVal[1] = 0;    		
	        		return retVal;
	            }else if(responseCode == 429){
	            	retVal = null;
					changeAPIKey(keyNum);
					//TODO System.exit(999);
	            	return retVal;
	            }
	            else {  // 에러 발생
	                br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
	                System.out.println("error1: "+apiURL);
	                System.exit(1);
	            }
	            String inputLine;
	            StringBuffer response = new StringBuffer();
	            while ((inputLine = br.readLine()) != null) {
	                response.append(inputLine);
	            }
	            br.close();
	           // System.out.println(response.toString());
	            
	           
	            JSONObject obj = (JSONObject) JSONValue.parse(response.toString());
				//System.out.println(obj);
				JSONObject location = (JSONObject) obj;
				location = (JSONObject) location.get("result");
				//System.out.println("location: "+ location);
				
				JSONArray arr = (JSONArray) location.get("items");
				//System.out.println("json: "+ arr);
				
				if(arr.isEmpty()){
					location = null;
					position = null;
					System.out.println("error2: "+apiURL);
					System.exit(2);
				}else{
					location = (JSONObject) arr.get(0);
					//System.out.println("items: "+ location);
					position = (JSONObject)(location.get("point"));
					
				}
		}catch (Exception e){
			System.out.println(e);
		}
		
		//System.out.println("coords: " + position.get("x")+ " " + position.get("y"));
		
		//Return coord values
		retVal = new double[2];
		retVal[0] = Double.parseDouble(String.valueOf(position.get("y")));
		retVal[1] =  Double.parseDouble(String.valueOf(position.get("x")));
		return retVal;
	}
	
	/**
	 * get lat lng of the land.
	 * @param conn
	 * @param SREG
	 * @param SEUB
	 * @param SSAN
	 * @param SBUN1
	 * @param SBUN2
	 * @return : lat lng of the land.
	 * **/
	public static double[] getPNUtoPosition(Connection conn, String SREG,String SEUB,String SSAN,String SBUN1,String SBUN2 ){
		
		double latLng[] = null;
		/*get rid of left zeros*/
		int sbun1_temp =  Integer.parseInt(SBUN1);
		int sbun2_temp = Integer.parseInt(SBUN2);
		/*replace SBUN2 0 to 1*/
		if (sbun2_temp == 0) sbun2_temp = 1;
		SBUN1 = Integer.toString(sbun1_temp);
		SBUN2 = Integer.toString(sbun2_temp);
		/*convert pnu code to korean address*/
		String korAddr = AddressToPosition.addr2Kor(conn, SREG, SEUB, SSAN,SBUN1, SBUN2);
		/*get lat lng of address*/
		latLng = AddressToPosition.getAdressToPosition(korAddr);
		return latLng;
	}
	
	/**
	 * Get distance between two latitudes and longitudes
	 * @param sLat: source latitude
	 * @param sLong : source longitude
	 * @param dLat: destination latitude
	 * @param dLong: destination longitude
	 * @return : distance in kilometers
	 */
	public static double getDistance_arc(double sLat, double sLong, double dLat, double dLong){  
        final int radius=6371009;

        //System.out.println(sLat+" "+sLong+" "+dLat+" "+dLong);
        
        double uLat=Math.toRadians(sLat-dLat);
        double uLong=Math.toRadians(sLong-dLong);
        double a = Math.sin(uLat/2) * Math.sin(uLat/2) + Math.cos(Math.toRadians(sLong)) * Math.cos(Math.toRadians(dLong)) * Math.sin(uLong/2) * Math.sin(uLong/2);  
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));  
        double distance = radius * c;

        return Double.parseDouble(String.format("%.3f", distance/1000));
    }
	
	public static void changeAPIKey(int curNum){
		if(curNum != (clientID.length -1))
			keyNum = curNum + 1;
		else 
			keyNum = 0;
		
		System.out.println("API key is changed: "+curNum+" -> "+keyNum);
	}
	
	/**
	 * Main method for testing
	 * @param args
	 */
	public static void main(String args[]){
		
		double test[]= {127.432,36.328};
		System.out.println(getDistance_arc(test[0], test[1], 127.442, 36.329));
		//test = AddressToPosition.getAdressToPosition("대전광역시 동구 천동 산 5-10");
		test = AddressToPosition.getAdressToPosition("대전광역시 유성구 봉명동 562-22");
		//test = AddressToPosition.getAdressToPosition("대전광역시 대덕구 신탄진동 711-50번지");
		
		
		
		System.out.println(""+test[0]+" "+ test[1]);
	}
	
	public static String addr2Kor(Connection con,String SREG, String SEUB,String SSAN, String SBUN1, String SBUN2){
		//PNU to Korean Address
		String addrInKor = null;
		String pnuToSGGQuery = "SELECT * FROM  "+DBConstants.DATABASE_NAME+"."+DBConstants.TABLE_NAME_DISTRICT+" where CODE = '" + SREG+SEUB + "'";
		java.sql.Statement sqlSt = null;
		try { sqlSt = con.createStatement();} catch (SQLException e1) {e1.printStackTrace();}
		ResultSet pnuToSGGrs;
		try {
			pnuToSGGrs = MySQLConn.getResultsFromDB(pnuToSGGQuery, con, sqlSt);
			if(pnuToSGGrs.next()){
				addrInKor = pnuToSGGrs.getString("SGG");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {sqlSt.close();} catch (SQLException e) {e.printStackTrace();}
		
		//code below is for naver map
		//check for mountain
		if(SSAN.equals("2")){
			//Mountain
			addrInKor = addrInKor+" " + "산";
		}
		
		//Check for 0 in SBUN2
		if(SBUN2.equals("0")){
			addrInKor = addrInKor+" " + SBUN1;
		}else{
			addrInKor = addrInKor+" " + SBUN1+"-"+SBUN2;
		}
		return addrInKor;
		
		//return addrInKor = addrInKor+" " + SBUN1+"-"+SBUN2;
	}

}


/*Daum!

	public static int keyNum = 0;
	public static String[] apiKeys= {
			"6f8a50ccaaa8082b482594086a9a0ba7",
			"fa5381e1c573bbf03115785a038f637d",
			"9f554b84a2548865420c2d8bbcd29c55",
			"fc55ea4a1d75e9378626f62a9ecea4fd"};
	
	public static double [] getAdressToPosition(String korAddress) {
		String urlStr = "";
		HttpURLConnection connection = null;
		BufferedReader reader = null;
		StringBuilder stringBuilder;
		double position[] = null;

		try {
			//urlStr = "http://apis.daum.net/local/geo/addr2coord?apikey=fc55ea4a1d75e9378626f62a9ecea4fd&output=json&q=" + URLEncoder.encode(korAddress, "utf-8");
			urlStr = "http://apis.daum.net/local/geo/addr2coord?apikey="+apiKeys[keyNum]+"&output=json&q=" + URLEncoder.encode(korAddress, "utf-8");
			//urlStr = "http://apis.daum.net/local/geo/addr2coord?apikey=a2f4c2af980631a2750e4dcccb803284&output=json&q=" + URLEncoder.encode(korAddress, "utf-8");
			
			
			//urlStr = "http://openapi.map.naver.com/api/geocode?key=93729c17a174f9acea28ba33a17984ef&encoding=utf-8&coord=latlng&output=json&query=" + korAddress; 
			//urlStr = "http://maps.googleapis.com/maps/api/geocode/xml?address="+URLEncoder.encode(korAddress, "utf-8")+"&language=ko&sensor=false";

			URL url = new URL(urlStr);
			
			Thread.sleep((long)(Math.random() * 1000));
			
			connection = (HttpURLConnection) url.openConnection();
			connection.setReadTimeout(3000);
			
			// read the output from the server
			if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
				//System.out.println("done");
				reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				stringBuilder = new StringBuilder();
	 
				String line = null;
				while ((line = reader.readLine()) != null)
				{
					stringBuilder.append(line + "\n");
				}
				
				Object obj = JSONValue.parse(stringBuilder.toString());
				//System.out.println(obj);
				JSONObject location = (JSONObject) obj;
				location = (JSONObject) location.get("channel");
				JSONArray arr = (JSONArray) location.get("item");
				if(arr.isEmpty()){
					String message = obj.toString();
					location = null;
					position = new double[1];
					//에러 처리용
					position[0] = Double.MAX_VALUE;
					//limit 넘었을 때 처리
					if(message.split("errorType").length>1){ 
						position = null;
						changeAPIKey(keyNum);
						//TODO System.exit(999);
					}
				}else{
					location = (JSONObject) arr.get(0);
					position = new double[2];
					//System.out.println("done");
					position[0] = Double.parseDouble(String.valueOf(location.get("lat")));
					position[1] = Double.parseDouble(String.valueOf(location.get("lng")));	

				}	
			} else {
				position = null;
				changeAPIKey(keyNum);
			}
		}  catch(java.net.SocketTimeoutException e){
			e.printStackTrace();
			System.out.println(e.toString()+" "+urlStr);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			System.out.println(e.toString()+" "+urlStr);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e.toString()+" "+urlStr);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
		
		return position;
	}
	
	public static void changeAPIKey(int curNum){
		if(curNum != (apiKeys.length -1))
			keyNum = curNum + 1;
		else 
			keyNum = 0;
		
		System.out.println("API key is changed: "+curNum+" -> "+keyNum);
	}
	
	public static double getDistance_arc(double sLat, double sLong, double dLat, double dLong){  
        final int radius=6371009;

        double uLat=Math.toRadians(sLat-dLat);
        double uLong=Math.toRadians(sLong-dLong);
        double a = Math.sin(uLat/2) * Math.sin(uLat/2) + Math.cos(Math.toRadians(sLong)) * Math.cos(Math.toRadians(dLong)) * Math.sin(uLong/2) * Math.sin(uLong/2);  
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));  
        double distance = radius * c;

        return Double.parseDouble(String.format("%.3f", distance/1000));
    }
	
	public static void main(String args[]){
		
		double test[]= {0.0,0.0};
		
		test = AddressToPosition.getAdressToPosition("���������� ���� õ�� �� 5-10");
		//test = AddressToPosition.getAdressToPosition("���������� ����� ��ź���� 711-50����");
		
		System.out.println(""+test[0]+" "+ test[1]);
	}
}
*/