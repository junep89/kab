package kab.updateData;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import kab.constants.DBConstants;
import kab.dbInterface.MySQLConn;
import kab.utils.DataUtil;
/**calculate distance attributes and append it to the original data list**/
public class AddDistAttr {

	final private static int Meter = 1000;
	/**name of target city**/
	static String CITY = "daejeon";

	/**modified data**/
	public List<ArrayList<String>> modifiedLandList = new ArrayList<ArrayList<String>>();
	/**all the names of the attributes**/
	public ArrayList<String> header = new ArrayList<String>();
	/**matching table of attribute and its index**/
	public HashMap<String, Integer> headerTable = new HashMap<String, Integer>();
	/**names of the distance attributes that need to load table from database**/

	public static void main(String[] args) {
		Connection conn = MySQLConn.connect("143.248.91.197:3306", DBConstants.DATABASE_NAME);

		/*test data 
		ArrayList<String> first_row =  new ArrayList<String>(Arrays.asList(DBConstants.COLUMN_NAME_YEAR, DBConstants.COLUMN_NAME_SREG, DBConstants.COLUMN_NAME_SEUB, DBConstants.COLUMN_NAME_SSAN, DBConstants.COLUMN_NAME_SBUN1, DBConstants.COLUMN_NAME_SBUN2, DBConstants.COLUMN_NAME_LAT,DBConstants.COLUMN_NAME_LNG ));
		ArrayList<String> test1 =  new ArrayList<String>(Arrays.asList("2014","30110","10100","1","0061","0061","36.32965351","127.432213"));
		ArrayList<String> test2 =  new ArrayList<String>(Arrays.asList("2014","30110","10100","1","0061","0062","36.35604161","127.3338443"));
		ArrayList<String> test3 =  new ArrayList<String>(Arrays.asList("2014","30110","10100","1","0061","0063","36.33764113","127.4768647"));
		ArrayList<ArrayList<String>> testLandList = new ArrayList<ArrayList<String>>(Arrays.asList(first_row, test1, test2,test3));
		 */

		Hashtable<String, Integer> inHeader = new Hashtable<String, Integer>();
		Hashtable<Integer, String> inHeaderByVal = new Hashtable<Integer, String>();
		List<String[]> testLandList =  new  ArrayList<String[]>();

		DataUtil.readCSV("data/gwangju/test2016_trimmed.csv", testLandList, inHeader, inHeaderByVal);

		List<ArrayList<String>> landList = DataUtil.convertInnerArray2List(testLandList);

		AddDistAttr ada = new AddDistAttr(conn, landList);

		//6. Save the results in CSV or DB
		String fileName = "test2016_trimmed_econ";
		DataUtil.writeCSV(ada.modifiedLandList, null, fileName);

		try {conn.close();} catch (SQLException e) {}
	}

	/**constructor
	 * @param conn
	 * @param landList
	 * @param findGPS true if the input data does not have gps data. 
	 * add distance attributes to landList
	 * **/
	public AddDistAttr(Connection conn, List<ArrayList<String>> landList){

		/**load distance attribute tables**/
		//HashMap<table_name,tableData>
		HashMap<String,ArrayList<Double[]>> dist_tables = loadTables(conn, DBConstants.DIST_ATTR_NAMES);

		/**read header**/
		ArrayList<String> header_of_table = landList.get(0);
		headerTable = readHeader(header_of_table);

		/**add names of the new attributes to header**/
		///ArrayList<String> newAttrNames = new ArrayList<String>(DBConstants.DIST_ATTR_NAMES);
		ArrayList<String> newAttrNames = new ArrayList<String>();

		
		/**check if the variables already exist*/
		boolean getGuri = !headerTable.containsKey(DBConstants.TABLE_NAME_GURI);
		boolean getGPS = !headerTable.containsKey(DBConstants.COLUMN_NAME_LAT);
		//TODO 
		//getGPS = false;
		//getGuri = false;
		if(getGuri){
			newAttrNames.add(DBConstants.TABLE_NAME_GURI);
		}
		if(getGPS){
			newAttrNames.add(DBConstants.COLUMN_NAME_LAT); 
			newAttrNames.add(DBConstants.COLUMN_NAME_LNG);
			System.out.println("get gps data by naver api: "+getGPS);
		}
		newAttrNames.add(DBConstants.COLUMN_NAME_PARLOT_SEP); 
		newAttrNames.add(DBConstants.COLUMN_NAME_RATE_4YEAR); 
		newAttrNames.add(DBConstants.COLUMN_NAME_POPUL); 
		newAttrNames.add(DBConstants.COLUMN_NAME_LANDTRANS); 
		
		addNewAttrToHeader(newAttrNames); 

		/*update the first row of the records*/
		/*deep copy header*/
		landList.set(0, (ArrayList<String>) header.clone());

		/**loop each record**/
		for(int i = 1; i < landList.size(); i++){ //the first element is header
			ArrayList<String> land = landList.get(i);

			String SREG = land.get(headerTable.get(DBConstants.COLUMN_NAME_SREG));
			String SEUB = land.get(headerTable.get(DBConstants.COLUMN_NAME_SEUB));
			String SSAN = land.get(headerTable.get(DBConstants.COLUMN_NAME_SSAN));
			String SBUN1 = land.get(headerTable.get(DBConstants.COLUMN_NAME_SBUN1));
			String SBUN2 = land.get(headerTable.get(DBConstants.COLUMN_NAME_SBUN2));

			String YEAR = land.get(headerTable.get("deal_ymd2"));
			System.out.println("SREG: " + SREG + " SEUB: " + SEUB + " SSAN: " + SSAN + " SBUN1: " + SBUN1 + " SBUN2: " + SBUN2);
			/**container for new attributes**/
			HashMap<String, Double> newAttr = new HashMap<String, Double>();

			/**get lat lng**/
			double[] latLng = new double[2];
			if(getGPS){
				/**get gps data by naver map api*/
				latLng = getPosition(conn, SREG,SEUB,SSAN,SBUN1,SBUN2);
				/**Add lat lng as new attributes**/
				newAttr.put("lat", latLng[0]);
				newAttr.put("lng", latLng[1]);
			}
			else{
				latLng[0] = 1.0;//Double.parseDouble(land.get(headerTable.get(DBConstants.COLUMN_NAME_LAT)));
				latLng[1] = 1.0;//Double.parseDouble(land.get(headerTable.get(DBConstants.COLUMN_NAME_LNG)));
				//System.out.println(latLng[0]+" "+latLng[1]);
			}

			/**get dist attributes**/
			//newAttr = getDistAttr(dist_tables, latLng, newAttr);

			if(getGuri){
				newAttr = getGuri(newAttr);
			}
			System.out.println("1");
			if (true) {

				/** econ vals **/
				String economyValQuery = "SELECT * FROM  " + "kirc_kab_debug."
						+ DBConstants.TABLE_NAME_ECONVAR + "_gwangju where  " + DBConstants.COLUMN_NAME_SREG + " = " + SREG
						+ " and  " + DBConstants.COLUMN_NAME_YEAR + "  = " + YEAR;
				newAttr.putAll(getEconVals(conn, economyValQuery));
				

			}
			/**append new dist attributes to the original data**/
			land = appendNewAttr(land, newAttrNames, newAttr);
			System.out.println("2");
			/**update data**/
			landList.set(i,land);
		}

		if(getGuri){
			/**get list of attribute to delete**/
			ArrayList<Integer> deleteColNumList = getAttrIndexList(DBConstants.TABLES_FOR_GURI);
			/**delete attributes**/
			landList = deleteAttr(landList, deleteColNumList);
		}
		/**modified landList*/
		modifiedLandList = landList;
	}
	public HashMap<String, Double> getEconVals(Connection conn, String query) {
		HashMap<String, Double> econValMap = new HashMap<String, Double>();
		java.sql.Statement sqlSt = null;
		try {
			sqlSt = conn.createStatement();
			System.out.println(query);
			ResultSet rsEconVal = MySQLConn.getResultsFromDB(query, conn, sqlSt);
			if (rsEconVal.next()) {
				econValMap.put(DBConstants.COLUMN_NAME_PARLOT_SEP,
						(double) rsEconVal.getInt(DBConstants.COLUMN_NAME_PARLOT_SEP));
				econValMap.put(DBConstants.COLUMN_NAME_RATE_4YEAR,
						(double) rsEconVal.getFloat(DBConstants.COLUMN_NAME_RATE_4YEAR));
				econValMap.put(DBConstants.COLUMN_NAME_POPUL, (double) rsEconVal.getInt(DBConstants.COLUMN_NAME_POPUL));
				econValMap.put(DBConstants.COLUMN_NAME_LANDTRANS,
						(double) rsEconVal.getInt(DBConstants.COLUMN_NAME_LANDTRANS));
			}
			rsEconVal.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			sqlSt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return econValMap;
	}
	/**
	 * read header of the table
	 * 
	 * @param header_of_table : the first low of the data
	 * @return HashMap<attribute_name, its_idex>: matching table of attribute names and column number
	 * **/
	public HashMap<String, Integer> readHeader(ArrayList<String> header_of_table){
		HashMap<String, Integer> attrColTable = new HashMap<String, Integer>(); 

		for(int i=0; i< header_of_table.size(); i++){
			header.add(header_of_table.get(i));
			attrColTable.put(header_of_table.get(i), i);
		}
		return attrColTable;
	}

	/**
	 * get lat lng of the land.
	 * @param conn
	 * @param SREG
	 * @param SEUB
	 * @param SSAN
	 * @param SBUN1
	 * @param SBUN2
	 * @return : lat lng of the land.
	 * **/
	public double[] getPosition(Connection conn, String SREG,String SEUB,String SSAN,String SBUN1,String SBUN2 ){

		double latLng[] = null;
		/*get rid of left zeros*/
		int sbun1_temp =  Integer.parseInt(SBUN1);
		int sbun2_temp = Integer.parseInt(SBUN2);
		/*replace SBUN2 0 to 1*/
		if (sbun2_temp == 0) sbun2_temp = 1;
		SBUN1 = Integer.toString(sbun1_temp);
		SBUN2 = Integer.toString(sbun2_temp);
		/*convert pnu code to korean address*/
		String korAddr = AddressToPosition.addr2Kor(conn, SREG, SEUB, SSAN,SBUN1, SBUN2);
		/*get lat lng of address*/
		latLng = AddressToPosition.getAdressToPosition(korAddr);
		return latLng;
	}

	/**
	 * load tables of the distance attributes
	 * @param conn
	 * @param table_names  ArrayList<table_name_to_load> 
	 * @return : all lat lng data of distance attribute tables.
	 * **/
	public  HashMap<String,ArrayList<Double[]>> loadTables(Connection conn, ArrayList<String> table_names){
		HashMap<String,ArrayList<Double[]>> tables = new  HashMap<String,ArrayList<Double[]>>();

		/*iterate each distance attribute*/
		for( int i = 0; i < table_names.size() ; i ++){
			ResultSet rs = null;
			java.sql.Statement sqlSt = null;
			ArrayList<Double[]> rsList = new ArrayList<Double[]>(); 

			String targetTableName = table_names.get(i);
			String query = "SELECT * FROM "+ DBConstants.DATABASE_NAME+"."+targetTableName+"_"+CITY;

			try { 
				sqlSt = conn.createStatement();
				rs = MySQLConn.getResultsFromDB(query, conn, sqlSt);

				/*get all positions of places in the distance attribute table*/
				while(rs.next()){
					Double[] LatLng = new Double[2];
					LatLng[0] = Double.valueOf(rs.getString("lat"));
					LatLng[1] = Double.valueOf(rs.getString("lng"));

					rsList.add(LatLng);
				}

				tables.put(table_names.get(i), rsList);
			}catch (SQLException e) {
				//TODO LOG warning
				System.out.println("[WARNING] there is no '"+targetTableName+"' table in database!");
			}finally{
				try {sqlSt.close();} catch (SQLException e) {e.printStackTrace();}
				try {rs.close();} catch (SQLException e) {e.printStackTrace();}
			}
		}
		return tables;
	}

	/**
	 * append new attributes to header
	 * @param newAttrNames: names of new attributes 
	 * **/
	public void addNewAttrToHeader( ArrayList<String> newAttrNames){
		for(int i = 0; i < newAttrNames.size() ; i++){
			header.add(newAttrNames.get(i));
		}
	}

	/**
	 * get values of distance attributes
	 * @param dist_tables: table of distance attributes 
	 * @return : hashmap of <new attribute, value>
	 * **/
	public HashMap<String, Double> getDistAttr(HashMap<String,ArrayList<Double[]>> dist_tables, double[] latLng, HashMap<String, Double> newAttr){

		Iterator<String> iterator = dist_tables.keySet().iterator();
		while (iterator.hasNext()) {
			String attrName = (String) iterator.next();
			ArrayList<Double[]> table = dist_tables.get(attrName);

			double distance = getMinDist(table, latLng);
			newAttr.put(attrName, distance);
		}

		return newAttr;
	}

	/**
	 * retrieve the minimum distance
	 * @param table: an array of positions to compare
	 * @return : target position to compare
	 * **/
	public double getMinDist(ArrayList<Double[]> table,  double[] latLng){
		ArrayList<Double> dist = new ArrayList<Double>();
		try {
			/**iterate each records in the table**/
			for( int i = 0; i < table.size() ; i++){
				Double vsLatLng[] = table.get(i);
				/*calculate distance from the target position*/
				double distance = AddressToPosition.getDistance_arc(latLng[0], latLng[1], vsLatLng[0], vsLatLng[1]);

				dist.add(distance);
			}
		} 
		catch (NumberFormatException e) {e.printStackTrace();}

		/*get minimum distance*/
		//numOfAdj(dist); <- 500m 이내에 몇 개 있는지 셀 때
		double minDist = Collections.min(dist)*Meter;
		return minDist;
	}

	public static int count = 0;
	public void numOfAdj(ArrayList<Double> dist){
		for( Double distance : dist){
			if(distance*Meter < 500){
				count++;
			}
		}

		System.out.println(count);
		count = 0;
		System.out.println();
	}


	/**
	 * get the minimum distance from samguri, neguri, oguri
	 * @param newAttr HashMap<new_attribute_name, its_value>
	 * @return : samguri, neguri, oguri are deleted & new attr 'guri' is added 
	 * **/
	public HashMap<String, Double> getGuri(HashMap<String, Double> newAttr){
		List<Double> values_for_guri = new ArrayList<Double>();

		for(int i = 0; i < DBConstants.TABLES_FOR_GURI.size(); i++){
			String attrName = DBConstants.TABLES_FOR_GURI.get(i);
			values_for_guri.add(newAttr.get(attrName));
			//newAttr.remove(attrName);
		}

		double minDist = Collections.min(values_for_guri);
		newAttr.put(DBConstants.TABLE_NAME_GURI, minDist);

		return newAttr;
	}


	/**
	 * append new attributes to the original data in order of newAttrNames
	 * @param originalData
	 * @param newAttrNames
	 * @param newAttr
	 * @return : modified original data
	 * **/
	public ArrayList<String> appendNewAttr(ArrayList<String> originalData, ArrayList<String> newAttrNames, HashMap<String, Double> newAttr){

		/*append new attributes order by newAttrNames*/
		for(int i = 0; i < newAttrNames.size(); i++) {
			String attrName = newAttrNames.get(i);
			if(newAttr.containsKey(attrName))
				originalData.add(newAttr.get(attrName).toString());
		}
		return originalData;
	}

	/**
	 * delete attributes from dataList
	 * @param dataList
	 * @param delColNumList index of column to delete
	 * @return : modified datalist
	 * **/
	public List<ArrayList<String>> deleteAttr(List<ArrayList<String>> dataList,  ArrayList<Integer> delColNumList){
		int num_of_attr = dataList.get(0).size();

		for(int i = 0; i < dataList.size(); i++){
			ArrayList<String> data = dataList.get(i);
			ArrayList<String> modifiedData = new ArrayList<String>();

			for ( int j = 0; j < num_of_attr ; j++) { 
				if(delColNumList.contains(j))
					continue; 
				modifiedData.add(data.get(j));
			}
			dataList.set(i, modifiedData);
		}

		/*update header*/
		header = new ArrayList<String>();
		headerTable = readHeader(dataList.get(0));

		return dataList;
	}

	/**
	 * get index of attribute
	 * @param delColList ArrayList<name_of_attribute_to_delete>
	 * @return : ArrayList<index_of_attribute_to_delete>
	 * **/
	public ArrayList<Integer> getAttrIndexList(ArrayList<String> delColList){
		ArrayList<Integer> delColNumList = new ArrayList<Integer>();

		for( String colName : delColList){
			for(int i = 0; i < header.size(); i ++){
				if(header.get(i).equals(colName))
					delColNumList.add(i);
			}
		}

		return delColNumList;
	}

	/* temp back up
	//move Target_Value to the end of the data
	//land = changeTargetValuePos(land);
	//update target value at the last position
	//changeHeaderPosition(DBConstants.CONUMN_NAME_TARGET_VALUE, num_of_attr-1);


	public ArrayList<String> changeTargetValuePos(ArrayList<String> data){

		//get target value
		int originalPosition = headerTable.get(DBConstants.CONUMN_NAME_TARGET_VALUE);
		String target_value = data.get(originalPosition);
		//remove target value from the data list
		data.remove(originalPosition);
		//put target value at the end of the data
		data.add(target_value);

		return data;
	}

	public void changeHeaderPosition(String attrName, int position){
		int originalPosition = headerTable.get(attrName);
		header.remove(originalPosition);
		header.add(attrName);
		headerTable.put(attrName, position);


		for(String name : header){
			System.out.println(name);
		}
		System.out.println();
		DataUtil.printHashMap(headerTable);

	}
	 */
}




